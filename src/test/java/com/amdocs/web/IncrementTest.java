package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.amdocs.Increment;

public class IncrementTest {
    @Test
    public void testCounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Decrease", 1, k);

    }

    @Test
    public void testCounter2() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Decrease2", 0, k);

    }
    
    @Test
    public void testCounter3() throws Exception {

        int k= new Increment().decreasecounter(3);
        assertEquals("Decrease3", -2, k);

    }
}

